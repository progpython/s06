# -*- coding: UTF-8 -*-
'''
Created on 2014-02-10
updated on 2022-07-09 -- Python 3.10
@author: Johnny Tsheke -- UQAM
validation d'un nombre entier
'''

nombre=input("Entrez un nombre entier svp\n")
while (not nombre.isdigit()):
    print(nombre,"n'est pas un nombre entier valide\n")
    nombre=input("Entrer un nombre entier svp\n")

nombre=eval(nombre)
print("Le nombre entré est: ",nombre)