# -*- coding: UTF-8 -*-
'''
Created on 2014-02-10
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
validation d'un nombre entier
'''


entier=False
nombre=0
while (not entier):
    nombre=input("Entrez un nombre entier svp \n")
    if(nombre.isdigit()):
        nombre=eval(nombre)
        entier=True #pour arreter la boucle
    else:  
        print(nombre,"n'est pas un nombre entier valide \n")
 
print("Le nombre entré est: ",nombre)