# -*- coding: UTF-8 -*-
'''
Created on 2022-07-09
updated on 2022-07-09 -- Python 3.10
@author: Johnny Tsheke
'''
LONGEUR_MIN=3 # longeur minimum que doit  avoir un nom
nomOk=False
nom=""
while (not nomOk):
        nom=input("Entrez votre nom svp ");
        if(nom.isalpha()): 
            if(len(nom)>=LONGEUR_MIN):
                nomOk=True # pour arreter la boucle
            else:
                print(nom," est trop court. Il faut au moin ", LONGEUR_MIN, " lettres.")
        else: 
            print(nom,"n'est pas un nom valide")
print("Le nom entré est: ",nom)