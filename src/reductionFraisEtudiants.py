# -*- coding: UTF-8 -*-
'''
Created on 10 févr. 2014
updated on 2022-07-09 -- Python 3.10
@author: Johnny Tsheke
'''
#calcul de réduction de 20 % pour les frais de scolarite de plus de 800.0
REDUCTION=0.2
BORNE_SUP=800.0
listeEtudiants=[] #liste d'accumulation
ajouter=True
oui=['O','o']
non=['N','n']

while (ajouter):
    #demande et validation de la reponse
    rep=input("Voulez-vous ajouter un etudiants?: \n")
    while((rep not in oui) and (rep not in non)):
        print("repondez par O(oui)/ N(non) svp \n")
        rep=input("Voulez-vous ajouter un etudiants?: \n")
    if(rep in non):
        ajouter=False #pour arreter d'ajouter
    else:
        #ici on ajoute un étudiant
        #demande et validation du nom
        nom=input("Entrez le nom de l'étudiant svp: \n")
        while(not nom.isalpha()):
            print(nom, "n'est pas un nom valide \n")
            nom=input("Entrez le nom de l'étudiant svp: \n")
        #demande et validation du prenom
        prenom=input("Entrez le prénom de l'étudiant svp: \n")
        while(not prenom.isalpha()):
            print(prenom, "n'est pas un prénom valide \n")
            prenom=input("Entrez le prénom de l'étudiant svp: \n")
        #demande et validation des frais
        fraisOk=False
        frais=0
        while(not fraisOk):
            try:
                frais=input("Entrez les frais de scolarité de l'étudiant svp: \n")
                frais=float(frais)
                if(frais>=0.0):
                    fraisOk=True #arret de la boucle de validation
                else:
                    print("Les frais de scolarité doit-être >=0 \n")
            except: 
                print("frais de scolarité ", frais," invalide \n")
        # A ce niveau on a le nom, prenom et les frais
        # on va garder les information de l'étudiant
        etudiant={"Prenom":prenom,"Nom":nom,"Frais":frais}
        #on acumule l'étudiant dans la liste
        listeEtudiants.append(etudiant)

#A ce niveau on a déja accumulé tous les étudiants dans la liste
#On va calculer les reductions 
for etud in listeEtudiants:
    if(etud["Frais"]>BORNE_SUP): #BORNE_SUP est une constante
        red=etud["Frais"]*REDUCTION # REDUCTION est une constante declaree au début
    else:
        red=0 
    etud.update({"Reduction":red})  

# a ce niveau on a fait tous les calculs et les mise à jour. on va afficher

print("\n Affichage des informations de chaque étudiant")
for etud in  listeEtudiants:
    print("---------------------------------------")
    print("Prénom: ",etud["Prenom"])
    print("Nom: ",etud["Nom"])
    print("Frais de scolarité: ",etud["Frais"])
    print("Réduction offerte: ",etud["Reduction"])   

#a ce niveau on a affiché toute la liste
print("\n Fin du programme")            
            