# -*- coding: UTF-8 -*-
'''
Created on 10 févr. 2014
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke -- UQAM
Validation nombres autoris
'''
entierAutorise=False
nombre=0
nbAutorises=[-7,-2,20,50,60,100]
print("voici les nombres autorisées",nbAutorises)
while (not entierAutorise):
    try :
        nombre=input("Entrez un nombre autorisé svp \n");
        nombre=int(nombre)
        if(nombre in nbAutorises): entierAutorise=True
        else: print(nombre,"n'est pas un nombre autorisé \n")
    except:
        print(nombre," n'est pas un nombre autorisé \n")
print("Le nombre entier entré est: ",nombre)