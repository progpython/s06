# -*- coding: UTF-8 -*-
'''
Created on 10 févr. 2014
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
'''

nomOk=False
nom=""
while (not nomOk):
        nom=input("Entrez votre nom svp ");
        if(nom.isalpha()): 
            nomOk=True # pour arreter la boucle
        else: 
            print(nom,"n'est pas un nom valide")
print("Le nom entré est: ",nom)