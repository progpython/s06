# -*- coding: UTF-8 -*-
'''
Created on 10 févr. 2014
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
Validation nombre entier positif
'''

entierPositif=False
nombre=0
while (not entierPositif):
    try :
        nombre=input("Entrez un nombre entier > 0 svp ");
        nombre=int(nombre)
        if(nombre>0): entierPositif=True
        else: print(nombre,"n'est pas un nombre entier positif")
    except:
        print(nombre," n'est pas un nombre entier")
print("Le nombre entier entré est: ",nombre)