# -*- coding: UTF-8 -*-
'''
Created on 10 févr. 2014
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke --UQAM
'''
reelPositif=False
nombre=0
while (not reelPositif):
    try :
        nombre=input("Entrez un nombre réel > 0 svp ");
        nombre=float(nombre) #accepte les entiers
        if(nombre>0.0): 
            reelPositif=True
        else: 
            print(nombre,"n'est pas un nombre réel positif")
    except:
        print(nombre," n'est pas un nombre réel")
print("Le nombre réel entré est: ",nombre)