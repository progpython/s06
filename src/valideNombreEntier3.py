# -*- coding: UTF-8 -*-
'''
Created on 2014-02-10
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
validation d'un nombre entier positif
'''

entierPositif=False
nombre=0
while (not entierPositif):
    nombre=input("Entrez un nombre entier strictement positif svp ")
    if(nombre.isdigit()):
        nombre=eval(nombre)
        if(nombre>0) :
            entierPositif=True #pour arreter la boucle
            #ATTETION if sans partie else
    else:  
        print(nombre,"n'est pas un nombre entier strictement positif valide ")
 
print("Le nombre entré est: ",nombre)